package com.NewTours.Excelutilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtilities {

	static XSSFWorkbook wb; // this is POI library referecne to a work book
	static XSSFSheet sheet; // this is POI library referencce to a sheet
     
	/* public String getData(String filePath) throws IOException{
		File src = new File(filePath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet=wb.getSheetAt(0);
		String data = sheet.getRow(0).getCell(1).getStringCellValue();
		//System.out.println("Value= " + data);
		return data;
	} */
	
	public static String getURL(String filePath, int Row, int Column, String Sheet) throws IOException{
		File src = new File(filePath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet=wb.getSheet(Sheet);
		String data = sheet.getRow(Row).getCell(Column).getStringCellValue();
		System.out.println("Value= " + data);
		return data;
	}
	
	public static void writeresult(String filePath, int Row, int Column, int Sheet, String Value) throws IOException{
		File src = new File(filePath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		
		sheet=wb.getSheetAt(Sheet);
		if(Sheet!=0)
		sheet.createRow(Row);
		sheet.getRow(Row).createCell(Column).setCellValue(Value);
		System.out.println("ROW###"+sheet.getRow(Row));
		
		FileOutputStream fout=new FileOutputStream(new File(filePath));
		wb.write(fout);
		System.out.println("Test Case Passed");
	
	}
	
	public static String getData(String filepath, int sheetnumber, int rownumber, int colnum) throws IOException {
		//System.out.println("Inside getData method");
		File src = new File(filepath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet = wb.getSheetAt(sheetnumber);
		System.out.println("Column###"+colnum);
		String data = sheet.getRow(rownumber).getCell(colnum)
				.getStringCellValue();
		return data;
	}
	
	
	public int getRowCount(int sheetIndex) {
		System.out.println("Inside getRowsCount method");
		int rowcount = wb.getSheetAt(sheetIndex).getLastRowNum();
		return rowcount;
	}
	
}	


//public static void addScreen(String filepath,String excelFilePath,int Sheet, int row,int column) throws IOException{
//	
//	
//	/* Read input PNG / JPG Image into FileInputStream Object*/
//    InputStream my_banner_image = new FileInputStream(filepath);
//    /* Convert picture to be added into a byte array */
//    byte[] bytes = IOUtils.toByteArray(my_banner_image);
//    File src = new File(excelFilePath);
//	FileInputStream fis = new FileInputStream(src);
//	wb = new XSSFWorkbook(fis);
//	sheet=wb.getSheetAt(Sheet);
//    /* Add Picture to Workbook, Specify picture type as PNG and Get an Index */
//    
//    int my_picture_id = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
//    /* Close the InputStream. We are ready to attach the image to workbook now */
//    my_banner_image.close();          
//    
//    /* Create the drawing container */
//    XSSFDrawing drawing = sheet.createDrawingPatriarch();
//    /* Create an anchor point */
//    CreationHelper helper = wb.getCreationHelper();
//    //XSSFClientAnchor my_anchor = new XSSFClientAnchor();
//    ClientAnchor anchor = helper.createClientAnchor();
//    anchor.setCol1(column);
//    anchor.setRow1(row);
//    /* Define top left corner, and we can resize picture suitable from there */
//    /* Invoke createPicture and pass the anchor point and ID */
//    XSSFPicture  my_picture = drawing.createPicture(anchor, my_picture_id);
//    /* Call resize method, which resizes the image */
//    my_picture.resize();            
//    /* Write changes to the workbook */
//    FileOutputStream fout=new FileOutputStream(new File(excelFilePath));
//	
//    wb.write(fout);
//    //my_workbook.write(out);
//    fout.close();	
//}



