package com.NewTours.testscripts;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class TestCase1 {
	public WebDriver driver;
	
  @Test
  public void openMyBlog() {
	driver.get("https://www.facebook.com/");
	driver.findElement(By.cssSelector("input[data-testid=royal_email]")).sendKeys("PJ");
	System.out.println("Hello there");
  }
  
  @BeforeClass
  public void beforeClass() {
	  
	  System.setProperty("webdriver.gecko.driver", "C:\\SELENIUM\\geckodriver.exe");
	  driver = new FirefoxDriver();
	  
  }

  @AfterClass
  public void afterClass() {
	  //driver.quit();
	  System.out.println("done");
  }
}
